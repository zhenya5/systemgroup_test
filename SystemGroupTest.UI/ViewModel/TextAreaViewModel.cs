﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using SystemGroupTest.UI.Abs.Common;
using SystemGroupTest.UI.Abs.Process;
using SystemGroupTest.UI.Entity.DTO;
using SystemGroupTest.UI.Process;
using SystemGroupTest.UI.View;

namespace SystemGroupTest.UI.ViewModel
{
    public class TextAreaViewModel : BaseViewModel
    {
        private ICommand _next;
        private string _textContent;
        private readonly ITextProcess _textProcess;

        public TextAreaViewModel()
        {
            _textProcess = new TextProcess();           
        }
        public ICommand Next =>
            _next ?? (_next=new RelayCommand(async obj =>
            {
                IDataResult<List<TextDataEntity>> result = 
                    await _textProcess.GetWordsAsync(TextContent);

                if (!result.Success)
                {
                    MessageBox.Show(result.Message);

                    return;                    
                }

                new ViewListResult(result.Data).ShowDialog();
            }));

        public  string TextContent
        {
            get => _textContent;
            set
            {

                _textContent = value;
                OnPropertyChanged(nameof(TextContent));
            }
        }
    }
}
