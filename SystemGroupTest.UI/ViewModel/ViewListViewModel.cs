﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using SystemGroupTest.UI.Entity.DTO;

namespace SystemGroupTest.UI.ViewModel
{
   public class ViewListViewModel : BaseViewModel
   {
       private ObservableCollection<TextDataEntity> _textDatas;

       public ViewListViewModel(List<TextDataEntity> textDataEntities)
       {

            _textDatas = new ObservableCollection<TextDataEntity>(textDataEntities);
       }


       public ObservableCollection<TextDataEntity> InfoByWords
       {
           get => _textDatas;
           set
           {
               _textDatas = value;
               OnPropertyChanged(nameof(InfoByWords));
           }
       }
    }
}
