﻿using System.Collections.Generic;
using SystemGroupTest.UI.Abs.Process;

namespace SystemGroupTest.UI.Process
{
    public class SpecialCharacter : ISpecialCharacter
    {
        public List<char> GetAllSpecialCharacterBuUtf16()
        {
            List<char> results = new List<char>();

            for (int i = 0; i <= 47; i++)
            {
                results.Add((char)i);
            }

            for (int i = 58; i <= 64; i++)
            {
                results.Add((char)i);
            }
            for (int i = 91; i <= 96; i++)
            {
                results.Add((char)i);
            }
            for (int i = 123; i <= 191; i++)
            {
                results.Add((char)i);
            }
            for (int i = 448; i <= 451; i++)
            {
                results.Add((char)i);
            }
            for (int i = 697; i <= 866; i++)
            {
                results.Add((char)i);
            }

            return results;
        }
    }
}
