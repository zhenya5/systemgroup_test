﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SystemGroupTest.UI.Abs.Common;
using SystemGroupTest.UI.Abs.Enums;
using SystemGroupTest.UI.Abs.Holder;
using SystemGroupTest.UI.Abs.Process;
using SystemGroupTest.UI.Entity.Common;
using SystemGroupTest.UI.Entity.DTO;

namespace SystemGroupTest.UI.Process
{
   public  class TextProcess : ITextProcess
    {
       private readonly ISpecialCharacter _specialCharacter;
        public TextProcess()
        {
            _specialCharacter = new SpecialCharacter();
        }
        public IDataResult<List<TextDataEntity>> GetWords(string text)
        {
            IDataResult<List<TextDataEntity>> methodResult =
                new DataResult<List<TextDataEntity>>();

            if (string.IsNullOrEmpty(text))
            {
                methodResult.Success = false;
                methodResult.Message = MessageHolder.GetMessage(MessageType.StringEmpty);

                return methodResult;
            }

            List<TextDataEntity> textDataEntities=
                text.Split(_specialCharacter.GetAllSpecialCharacterBuUtf16().ToArray(),
                StringSplitOptions.RemoveEmptyEntries)
                    .Select(p=>p.ToLower())
                    .GroupBy(p=>p)
                .Select(p=> new TextDataEntity()
                        {
                            Word = p.Key,
                            Count = p.ToList().Count()

                        }).ToList();
            methodResult.Data = textDataEntities;
            methodResult.Success = true;

            return methodResult;

        }
        public async Task<IDataResult<List<TextDataEntity>>> GetWordsAsync(string text)
        {
          return  await Task.Run(() =>
            {
                IDataResult<List<TextDataEntity>> methodResult =
                    new DataResult<List<TextDataEntity>>();

                if (string.IsNullOrEmpty(text))
                {
                    methodResult.Success = false;
                    methodResult.Message = MessageHolder.GetMessage(MessageType.StringEmpty);

                    return methodResult;
                }


                List<TextDataEntity> textDataEntities =
                    text.Split(_specialCharacter.GetAllSpecialCharacterBuUtf16().ToArray(),
                            StringSplitOptions.RemoveEmptyEntries)
                        .Select(p => p.ToLower())
                        .GroupBy(p => p)
                        .Select(p => new TextDataEntity()
                        {
                            Word = p.Key,
                            Count = p.ToList().Count()

                        }).ToList();
                methodResult.Data = textDataEntities;
                methodResult.Success = true;

                return methodResult;
            });
        }
    }
}
