﻿using System.Collections.Generic;
using SystemGroupTest.UI.Abs.Enums;

namespace SystemGroupTest.UI.Abs.Holder
{
    public static  class MessageHolder
    {
        private static readonly Dictionary<MessageType, string> DictionaryMessages =
            new Dictionary<MessageType, string>();

        static MessageHolder()
        {
            Init();
        }

        private static void Init()
        {
            DictionaryMessages.Add(MessageType.StringEmpty, "Text box is empty, please enter the text");
            DictionaryMessages.Add(MessageType.Default, "Sorry, something went wrong");
        }

        public static string GetMessage(MessageType messageType)
        {
            if (DictionaryMessages.ContainsKey(messageType))
            {
                return DictionaryMessages[messageType];
            }

            return null;
        }
    }
}
