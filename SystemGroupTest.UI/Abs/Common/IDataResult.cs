﻿

namespace SystemGroupTest.UI.Abs.Common
{
    public interface IDataResult<T>:IResult
    {
        T Data { get; set; }
    }
}
