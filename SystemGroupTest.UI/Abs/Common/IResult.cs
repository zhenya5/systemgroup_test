﻿

namespace SystemGroupTest.UI.Abs.Common
{
    public interface IResult
    {
         bool Success { get; set; }
         string Message { get; set; }
    }
}
