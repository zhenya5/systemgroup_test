﻿using System.Collections.Generic;

namespace SystemGroupTest.UI.Abs.Process
{
    public  interface ISpecialCharacter
    {
        List<char> GetAllSpecialCharacterBuUtf16();
    }
}
