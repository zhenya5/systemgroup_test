﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SystemGroupTest.UI.Abs.Common;
using SystemGroupTest.UI.Entity.DTO;

namespace SystemGroupTest.UI.Abs.Process
{
    public interface ITextProcess
    {
        IDataResult<List<TextDataEntity>> GetWords(string text);
        Task<IDataResult<List<TextDataEntity>>> GetWordsAsync(string text);
    }
}
