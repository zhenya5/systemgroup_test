﻿using SystemGroupTest.UI.ViewModel;

namespace SystemGroupTest.UI.Entity.DTO
{
   public class TextDataEntity : BaseViewModel
    {
        public string Word { get; set; }
        public int Count { get; set; }
    }
}
