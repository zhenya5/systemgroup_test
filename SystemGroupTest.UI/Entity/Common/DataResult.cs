﻿using SystemGroupTest.UI.Abs.Common;

namespace SystemGroupTest.UI.Entity.Common
{
    public class DataResult<T> :Result, IDataResult<T>
    {
        public T Data { get; set; }
    }
}
