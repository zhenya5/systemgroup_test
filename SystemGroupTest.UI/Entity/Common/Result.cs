﻿using SystemGroupTest.UI.Abs.Common;

namespace SystemGroupTest.UI.Entity.Common
{
    public class Result : IResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
